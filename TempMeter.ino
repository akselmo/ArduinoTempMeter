/* Temperature meter with LCD display using AM2301 */

#include <LiquidCrystal.h>
#include <DHT.h>

//devices
LiquidCrystal lcd(12, 11, 5, 4, 3, 2);
DHT dht(7, DHT21);

//vars
float hum; //humidity
float temp; //temperature

//protos
void printValue(char* valueText, float value, char unitChar);

void setup()
{
  // set up the LCD's number of columns and rows:
  lcd.begin(16, 2);
  // Print a message to the LCD.
  lcd.print("Booting up...");
  dht.begin();
}

void loop()
{
  hum = dht.readHumidity();
  temp = dht.readTemperature();

  //First row
  lcd.setCursor(0, 0);
  printValue("Temp: ", temp, " C");

  //Second row
  lcd.setCursor(0, 1);
  printValue("Hum: ", hum, " %");

  delay(2000); //Delay 2 sec.
}

void printValue(char* valueText, float value, char* unitChar)
{
  lcd.print(valueText);
  lcd.print(value);
  lcd.print(unitChar);
}
