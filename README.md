# ArduinoTempMeter

Very simple temperature and humidity meter using Arduino Uno
and the AM2301 temperature and humidity sensor.

# Image

![Temp meter in action](./in_action.jpg)

# What

The code prints every two seconds whats happening in the sensor to the screen,
in this case humidity and temperature.

I made this for measuring my bearded dragons terrarium for humidity.
The terrarium has a hole on top of it so I can put the sensor in from there. :)

# How-to

I basically followed this tutorial here for the LCD screen itself:

[Liquid Crystal Displays (LCD) with Arduino](https://docs.arduino.cc/learn/electronics/lcd-displays)

Then I just connected the AM2301 into one of the free digital pins (in my case 7) and
the 5V and GND.

Then just run the `TempMeter.ino` from Arduino IDE 2.

Make sure to install the `DHT sensor library by Adafruit` library, so
the temp sensor can work.

Super simple thing but it was fun to make.